/* minblorb.c: only those portions of the blorb spec needed for cover art

   Copyright 2011 Lewis Gentry.

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <string.h>

/* File format reference: <http://www.eblong.com/zarf/blorb/>. */

/* Important offsets: */

#define RESOURCE_INDEX 0x0C /* Beginning of resource index. */
#define RESOURCE_COUNT 0x14 /* Number of resources. */

/* Does this file look like a blorb? */

int verify(FILE *blorb)
{
  char signature[12];

  rewind(blorb);
  return fread(signature, sizeof(char), 12, blorb) == 12
    && memcmp(signature + 0, "FORM", 4) == 0
    && memcmp(signature + 8, "IFRS", 4) == 0;
}

/* Convert a four-byte word (big-endian) to an integer. */

unsigned int int32_read(unsigned char *word)
{
  return word[0] * 16777216 + word[1] * 65536 + word[2] * 256 + word[3];
}

/* As above, but rounded up to an even number per the IFF spec. */

unsigned int int32_padded(unsigned char *word)
{
  unsigned int i;

  i = int32_read(word);
  return i % 2 ? i + 1 : i;
}

/* Does the stream contain a frontispiece chunk? If so, store its
   resource number in the provided pointer. */

int fspc(FILE *blorb, unsigned int *resource)
{
  unsigned char buffer[8] = { 0, 0, 0, 0, 0, 0, 0, RESOURCE_INDEX };
  unsigned char target[4];

  rewind(blorb);
  while (fseek(blorb, int32_padded(buffer + 4), SEEK_CUR) == 0
         && fread(buffer, sizeof(char), 8, blorb) == 8) {
    if (memcmp(buffer, "Fspc", 4) == 0
        && fread(target, sizeof(char), 4, blorb) == 4) {
      *resource = int32_read(target);
      return 1;
    }
  }
  return 0;
}

/* Does the picture with this resource number exist? If so, return its
   address. */

unsigned int findpict(FILE *blorb, unsigned int id)
{
  unsigned char buffer[12];
  int resources;

  if (fseek(blorb, RESOURCE_COUNT, SEEK_SET) == 0
      && fread(buffer, sizeof(char), 4, blorb) == 4) {
    resources = int32_read(buffer);
    while (resources-- && fread(buffer, sizeof(char), 12, blorb) == 12) 
      if (memcmp(buffer, "Pict", 4) == 0 && int32_read(buffer + 4) == id)
        return int32_read(buffer + 8);
  }
  return 0;
}

/* How long is the chunk at this offset? (As a useful side effect, the
   stream is now positioned at the beginning of the chunk data.) */

unsigned int chunklength(FILE *blorb, unsigned int offset)
{
  unsigned char buffer[8];

  return fseek(blorb, offset, SEEK_SET) == 0
    && fread(buffer, sizeof(char), 8, blorb) == 8
    ? int32_read(buffer + 4) : 0;
}
