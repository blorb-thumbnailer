/* main.c: thumbnail a blorb using embedded cover art

   Copyright 2011 Lewis Gentry.

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "main.h"
#include <stdlib.h>

#define DEFAULTSIZE 128 /* Maximum thumbnail size in pixels square. */

/* Main returns EXIT_SUCCESS only if we successfully produce the
   requested thumbnail. */

int main(int argc, char **argv)
{
  FILE *blorb;
  unsigned int size, id, chunk, length;

  size = argc > 3 ? strtol(argv[3], NULL, 0) : DEFAULTSIZE;
  if (size < 1 || argc < 3) {
    printf("Usage: %s <input> <output> [size]\n", argv[0]);
    return EXIT_FAILURE;
  }

  blorb = fopen(argv[1], "rb");
  if (blorb && verify(blorb) && fspc(blorb, &id)) {
    chunk = findpict(blorb, id);
    length = chunklength(blorb, chunk);
    if (chunk && length && thumb(blorb, length, argv[2], size))
      return EXIT_SUCCESS;
  }
  return EXIT_FAILURE;
}
