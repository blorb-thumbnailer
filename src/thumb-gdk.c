/* thumb-gdk.c: load, scale, and save the image using a GDK Pixbuf

   Copyright 2011 Lewis Gentry.

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <stdio.h>

/* As soon as we learn the dimensions of the existing cover, this
   callback function snaps into play to set the dimensions of the
   thumbnail. */

void scale(GdkPixbufLoader *self, gint width, gint height, gpointer reqsize)
{
  gint max = GPOINTER_TO_INT(reqsize);

  if (width == height)
    gdk_pixbuf_loader_set_size(self, max, max);
  else if (width < height)
    gdk_pixbuf_loader_set_size(self, width * max / height, max);
  else
    gdk_pixbuf_loader_set_size(self, max, height * max / width);
}

/* Feed a quantity of bytes to dest (an initialized PixbufLoader) from
   source (a stream open for reading) and return status. */

int fill(GdkPixbufLoader *dest, FILE *source, unsigned int bytes_remaining)
{
  unsigned int stride = 4096;
  guchar buffer[stride];

  for (; bytes_remaining; bytes_remaining -= stride) {
    if (bytes_remaining < stride)
      stride = bytes_remaining;
    if (fread(buffer, sizeof(guchar), stride, source) != stride
        || !gdk_pixbuf_loader_write(dest, buffer, stride, NULL)) {
      gdk_pixbuf_loader_close(dest, NULL);
      return 0;
    }
  }
  return gdk_pixbuf_loader_close(dest, NULL);
}

/* Read, scale, and save an image. Return true if no mishaps. */

int thumb(FILE *stream, unsigned int n, char *pathname, int requested_size)
{
  GdkPixbufLoader *loader;
  GdkPixbuf *pixbuf;
  int status = 0;

  g_type_init(); /* Required before doing gAnything. */
  loader = gdk_pixbuf_loader_new();
  g_signal_connect(loader, "size_prepared", (GCallback) scale,
                   GINT_TO_POINTER(requested_size));

  if (fill(loader, stream, n)) {
    pixbuf = gdk_pixbuf_loader_get_pixbuf(loader);
    if (pixbuf)
      status = gdk_pixbuf_save(pixbuf, pathname, "png", NULL, NULL);
  }
  g_object_unref(loader);
  return status;
}
