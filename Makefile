GNOME = 3

PREFIX = /usr/local
BINDIR = $(DESTDIR)$(PREFIX)/bin
MANDIR = $(DESTDIR)$(PREFIX)/man/man1
DEFDIR = $(DESTDIR)$(PREFIX)/share/thumbnailers

src/blorb-thumbnailer :
	cd src; $(MAKE)

clean distclean :
	cd src; $(MAKE) clean

install : src/blorb-thumbnailer
	install -d $(BINDIR)
	install src/blorb-thumbnailer $(BINDIR)

	install -d $(MANDIR)
	install man/blorb-thumbnailer.1 $(MANDIR)

	PATH=$$PATH:mime xdg-mime install --novendor mime/blorb.xml

	install -d $(DEFDIR)
	install def/blorb.thumbnailer $(DEFDIR)

ifeq ($(GNOME), 2)
    ifeq (home, $(word 1,$(subst /, ,$(PREFIX))))
	gconftool-2 --install-schema-file=def/blorb-thumbnailer.schemas
    else
	GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source` \
	gconftool-2 --makefile-install-rule def/blorb-thumbnailer.schemas
    endif
	killall -HUP gconfd-2
endif

uninstall :
	rm -f $(BINDIR)/blorb-thumbnailer
	rm -f $(MANDIR)/blorb-thumbnailer.1
	rm -f $(DEFDIR)/blorb.thumbnailer

	PATH=$$PATH:mime xdg-mime uninstall mime/blorb.xml

ifeq ($(GNOME), 2)
    ifeq (home, $(word 1,$(subst /, ,$(PREFIX))))
	gconftool-2 --recursive-unset \
	/desktop/gnome/thumbnailers/application@x-blorb
    else
	GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source` \
	gconftool-2 --makefile-uninstall-rule def/blorb-thumbnailer.schemas
    endif
	killall -HUP gconfd-2
endif
